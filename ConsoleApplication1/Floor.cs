﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleApplication1;
// ReSharper disable InconsistentNaming

namespace ConsoleApplication1 {
    public class Floor {
        private bool[,] midpointRestrictionMap;

        private List<Plot> plotList;
        private List<Hall> halls;
        public List<int[]> roomWalls;
        
        public char[,] map;
        
        
        private int height;
        private int width;
        private int minRoomHeight;
        private int minRoomWidth;
        private int maxRoomHeight;
        private int maxRoomWidth;
        private int numberOfRooms;
        private float splitVariable;
        private Random random;

        public Floor() {
        }

        public Floor(int minRoomHeight, int minRoomWidth, int maxRoomHeight, int maxRoomWidth, int numberOfRooms, float splitVariable, int padding) {                
            height = (maxRoomHeight + 4 + padding) * 4;
            width = (maxRoomWidth + 4 + padding) * 4;
            this.minRoomHeight = minRoomHeight;
            this.minRoomWidth = minRoomWidth;
            this.maxRoomHeight = maxRoomHeight;
            this.maxRoomWidth = maxRoomWidth;
            this.numberOfRooms = numberOfRooms;
            this.splitVariable = splitVariable;
            
            random = new Random();
            plotList = new List<Plot>();
            roomWalls = new List<int[]>();
            
            midpointRestrictionMap = new bool[height, width];
            map = new char[height, width];
            

            // Make sure to organize all of the data generated in a way that's easy to grab from here.
            // Have lists for all of the room data and also store all of the organized data onto 2d arrays as maps
            // for use with unity.
            
            // Generate the floor data
            generatePlots();
            generateRooms();
            connectHalls(plotList[0]);

            // Clean up
            removeDeadEnds();
            removeFigureEightTwists();
            //removeDoubleConnections();
            
            
            defineEntrances();
            
            // generate the walls and etc
            generateWalls();
            
            // debug/print
            drawDebugRooms();
            printMap();

            
            
            // print to text file          
            string[] filemap = new string[map.GetLength(0)];
            for (int row = 0; row < map.GetLength(0); row++) {
                for (int col = 0; col < map.GetLength(1); col++) {
                    filemap[row] += map[row,col] + " ";
                }
            }
            using (System.IO.StreamWriter file = 
                new System.IO.StreamWriter(@"C:\Users\khowin\Desktop\map.txt"))
            {
                for (int row = 0; row < filemap.GetLength(0); row++) {
                    file.WriteLine(filemap[row]);   
                }
            }
        }

        private void defineEntrances() {
            foreach (Room room in plotList[0].rooms) {
                foreach (Hallmark hallmark in room.hallmarks) {
                    if (hallmark.connected) {
                        if (hallmark.type == HallmarkType.North) {
                            room.entrances.Add(new int[] {hallmark.row - 1, hallmark.col});
                        }
                        else if (hallmark.type == HallmarkType.South) {
                            room.entrances.Add(new int[] {hallmark.row + 1, hallmark.col});
                        }
                        else if (hallmark.type == HallmarkType.East) {
                            room.entrances.Add(new int[] {hallmark.row, hallmark.col + 1});
                        }
                        else if (hallmark.type == HallmarkType.West) {
                            room.entrances.Add(new int[] {hallmark.row, hallmark.col - 1});
                        }
                    }
                }
            }
        }

        private void generateWalls() {
            bool occupied = false;
            
            // walls for rooms
            foreach (Room room in plotList[0].rooms) {
                // UR
                room.walls.Add(new int[] {room.roomRow - 1, room.roomCol + room.roomWidth});
                // UL
                room.walls.Add(new int[] {room.roomRow - 1, room.roomCol - 1});
                // DL
                room.walls.Add(new int[] {room.roomRow + room.roomHeight, room.roomCol - 1});
                // DR
                room.walls.Add(new int[] {room.roomRow + room.roomHeight, room.roomCol + room.roomWidth});
                
                // R
                for (int row = room.roomRow; row < room.roomRow + room.roomHeight; row++) {
                    foreach (int[] entrance in room.entrances) {
                        if (row == entrance[0] && room.roomCol + room.roomWidth == entrance[1]) {
                            occupied = true;
                            break;
                        }
                    }
                    if (!occupied) {
                        room.walls.Add(new int[]{row, room.roomCol + room.roomWidth});
                    }
                    occupied = false;
                }
                // D
                for (int col = room.roomCol; col < room.roomCol + room.roomWidth; col++) {
                    foreach (int[] entrance in room.entrances) {
                        if (col == entrance[1] && room.roomRow + room.roomHeight == entrance[0]) {
                            occupied = true;
                            break;
                        }                     
                    }
                    if (!occupied) {
                        room.walls.Add(new int[] {room.roomRow + room.roomHeight, col});
                    }
                    occupied = false;
                }
                
                // L
                for (int row = room.roomRow - 1; row < room.roomRow + room.roomHeight; row++) {
                    foreach (int[] entrance in room.entrances) {
                        if (row == entrance[0] && room.roomCol - 1 == entrance[1]) {
                            occupied = true;
                            break;
                        }                       
                    }
                    if (!occupied) {
                        room.walls.Add(new int[] {row, room.roomCol - 1});
                    }
                    occupied = false;
                }
                
                // U
                for (int col = room.roomCol; col < room.roomCol + room.roomWidth; col++) {
                    foreach (int[] entrance in room.entrances) {
                        if (col == entrance[1] && room.roomRow - 1 == entrance[0]) {
                            occupied = true;
                            break;
                        }                       
                    }
                    if (!occupied) {
                        room.walls.Add(new int[] {room.roomRow - 1, col});
                    }
                    occupied = false;
                }
            }
            
            //walls for halls
            // 0 is row and 1 is col
            foreach (Hall hall in plotList[0].halls) {
                for (int hallpoint = 0; hallpoint < hall.hallpoints.Count; hallpoint++) {                  
                    int[] prev;
                    int[] current;
                    int[] next;
                    
                    if (hallpoint == 0) {
                        prev = new int[] {hall.from.row, hall.from.col};
                        current = hall.hallpoints[hallpoint];
                        next = hall.hallpoints[hallpoint + 1];
                    }
                    else if (hallpoint > 0 && hallpoint < hall.hallpoints.Count - 1) {
                        prev = hall.hallpoints[hallpoint - 1];
                        current = hall.hallpoints[hallpoint];
                        next = hall.hallpoints[hallpoint + 1];
                    }
                    else {
                        prev = hall.hallpoints[hallpoint - 1];
                        current = hall.hallpoints[hallpoint];
                        next = new int[] {hall.to.row, hall.to.col};                      
                    }
                    hall.walls.AddRange(getHallWallsAtPoint(prev, current, next));

                    foreach (int[] pos in hall.walls) {
                        map[pos[0], pos[1]] = '▓';
                    }
                }                   
            }
            
            
        }

        private List<int[]> getHallWallsAtPoint(int[] prev, int[] current, int[] next) {
            List<int[]> walls = new List<int[]>();
            
            //   >   
            // * * *
            if (prev[0] == current[0] && prev[1] < current[1] &&
                current[0] == next[0] && current[1] < next[1]) {
                
                walls.Add(new int[]{current[0] - 1, current[1]});
                walls.Add(new int[]{current[0] + 1, current[1]});
            }
            
            //   >
            // * * v 
            //   * 
            else if (prev[0] == current[0] && prev[1] < current[1] &&
                current[0] < next[0] && current[1] == next[1]) {
                
                walls.Add(new int[]{current[0] - 1, current[1]});
                walls.Add(new int[]{current[0] - 1, current[1] + 1});
                walls.Add(new int[]{current[0], current[1] + 1});
                
            }
            
            //   * 
            // * * ^
            //   >
            else if (prev[0] == current[0] && prev[1] < current[1] &&
                current[0] > next[0] && current[1] == next[1]) {
                
                walls.Add(new int[]{current[0] + 1, current[1]});
                walls.Add(new int[]{current[0] + 1, current[1] + 1});
                walls.Add(new int[]{current[0], current[1] + 1});
            }
            
            // * 
            // * ^
            // * 
            else if (prev[0] > current[0] && prev[1] == current[1] &&
                current[0] > next[0] && current[1] == next[1]) {
                
                walls.Add(new int[]{current[0], current[1] - 1});
                walls.Add(new int[]{current[0], current[1] + 1});
            }
            
            //   < 
            // * * ^
            //   * 
            else if (prev[0] > current[0] && prev[1] == current[1] &&
                current[0] == next[0] && current[1] > next[1]) {
                
                walls.Add(new int[]{current[0], current[1] + 1});
                walls.Add(new int[]{current[0] - 1, current[1] + 1});
                walls.Add(new int[]{current[0] - 1, current[1]});
            }
                       
            //   >
            // ^ * *
            //   *
            else if (prev[0] > current[0] && prev[1] == current[1] &&
                current[0] == next[0] && current[1] < next[1]) {
                
                walls.Add(new int[]{current[0], current[1] - 1});
                walls.Add(new int[]{current[0] - 1, current[1] - 1});
                walls.Add(new int[]{current[0] - 1, current[1]});
            }
            
            //   <
            // * * *
            else if (prev[0] == current[0] && prev[1] > current[1] &&
                current[0] == next[0] && current[1] > next[1]) {
                
                walls.Add(new int[]{current[0] - 1, current[1]});
                walls.Add(new int[]{current[0] + 1, current[1]});               
            }
            
            //   < 
            // v * *
            //   *
            else if (prev[0] == current[0] && prev[1] > current[1] &&
                current[0] < next[0] && current[1] == next[1]) {
                
                walls.Add(new int[]{current[0] - 1, current[1]});
                walls.Add(new int[]{current[0] - 1, current[1] - 1});
                walls.Add(new int[]{current[0], current[1] - 1});
            }
            
            //   *
            //   * *
            // ^ < 
            else if (prev[0] == current[0] && prev[1] > current[1] &&
                current[0] > next[0] && current[1] == next[1]) {
                
                walls.Add(new int[]{current[0] + 1, current[1]});
                walls.Add(new int[]{current[0] + 1, current[1] - 1});
                walls.Add(new int[]{current[0], current[1] - 1});
            }
                      
            // *  
            // * v
            // *  
            else if (prev[0] < current[0] && prev[1] == current[1] &&
                current[0] < next[0] && current[1] == next[1]) {
                
                walls.Add(new int[]{current[0], current[1] - 1});
                walls.Add(new int[]{current[0], current[1] + 1});
            }
            //   * 
            // v * *
            // > 
            else if (prev[0] < current[0] && prev[1] == current[1] &&
                current[0] == next[0] && current[1] < next[1]) {
                
                walls.Add(new int[]{current[0], current[1] - 1});
                walls.Add(new int[]{current[0] + 1, current[1] - 1});
                walls.Add(new int[]{current[0] + 1, current[1]});
            }
            
            //   * 
            // * * v
            //     <
            else if (prev[0] < current[0] && prev[1] == current[1] &&
                current[0] == next[0] && current[1] > next[1]) {
                
                walls.Add(new int[]{current[0], current[1] + 1});
                walls.Add(new int[]{current[0] + 1, current[1] + 1});
                walls.Add(new int[]{current[0] + 1, current[1]});
            }
            
            return walls;
        }


        private int getNumberOfConnections(Room room) {
            int count = 0;
                
            if (room.hallmarkNorth.connected) {
                count++;
            }
            if (room.hallmarkSouth.connected) {
                count++;
            }
            if (room.hallmarkEast.connected) {
                count++;
            }
            if (room.hallmarkWest.connected) {
                count++;
            }

            return count;
        }

        private void removeFigureEightTwists() {
            List<Hall> remove = new List<Hall>();
            
            foreach (Hall hallA in plotList[0].halls) {
                foreach (Hall hallB in plotList[0].halls) {
                    if (hallA != hallB) {
                        if ((hallA.to.room == hallB.to.room || hallA.to.room == hallB.from.room) &&
                            (hallA.from.room == hallB.to.room || hallA.from.room == hallB.from.room)) {
                            int choose = random.Next() % 2;

                            if (choose == 0) {
                                remove.Add(hallA);
                                goto Break;
                            }
                            else {
                                remove.Add(hallB);
                                goto Break;
                            }
                        }
                    }
                }
            }
            Break:

            foreach (Hall hall in remove) {
                hall.from.connected = false;
                hall.to.connected = false;
                plotList[0].halls.Remove(hall);
                removeFigureEightTwists();
            }
        }

        
        // Removes the halls that have 3 hallmarks which aren't connected to anything.
        // After removing the hall it runs the function recursively to remove any extra dead ends
        // created from removing a hall.
        private void removeDeadEnds() {
            List<Hall> deadEnds = new List<Hall>();
            List<Room> deadRooms = new List<Room>();
            int count;
            
            foreach (Hall hall in plotList[0].halls) {              
                if (hall.to.room.isHallpoint) {
                    Room to = hall.to.room;
                    count = getNumberOfConnections(to);
                    
                    if (count == 1) {
                        deadEnds.Add(hall);
                        deadRooms.Add(to);
                        hall.from.connected = false;
                    }                   
                }
            }
            
            foreach (Hall hall in plotList[0].halls) {              
                if (hall.from.room.isHallpoint) {
                    Room from = hall.from.room;
                    count = getNumberOfConnections(from);
                    
                    if (count == 1) {
                        deadEnds.Add(hall);
                        deadRooms.Add(from);
                        hall.to.connected = false;
                    }                   
                }
            }

            
            
            if (deadEnds.Any()) {
                foreach (Room room in deadRooms) {
                    plotList[0].rooms.Remove(room);
                }
                foreach (Hall hall in deadEnds) {                  
                    plotList[0].halls.Remove(hall);
                    removeDeadEnds();
                }
            }
        }
        
         private void generatePlots() {
             SplitOrientation splitOrientation;
             Plot rootPlot;
             int depth;
             int row;
             int col;
             int height;
             int width;
             int depthTracker;

            // initialize root plot
             depth = 0;
             row = 0;
             col = 0;
             height = this.height;
             width = this.width;
             rootPlot = new Plot(depth, row, col, height, width);      
             
            // initialize depthtracker
            depthTracker = 0;
            
             // add the initial lot to the list to act as the root node of the tree that is about to be built
             plotList.Add(rootPlot);
             
            // randomly pick a split orientation to start with
            splitOrientation = random.Next() % 2 == 0 ? SplitOrientation.Vertical : SplitOrientation.Horizontal;
       
            // The index is tracking the number of splits. The number of rooms generated is when the total number of splits == numberOfBuildSpaces - 1
            int buildSpaces = 16;
            for (int split = 0; split < buildSpaces - 1; split++) {
                // if the current plot's depth is different from the depth tracking variable, switch the split orientation           
                splitOrientation = depthTracker == plotList[split].depth ? splitOrientation : switchOrientation(splitOrientation);
                
                // set ID of current plot
                plotList[split].ID = split;
                
                // split the plot, creating children
                splitPlot(plotList[split], splitOrientation);
                
                // pass the current split orientation to the plot
                plotList[split].splitOrientation = splitOrientation;
                
                // update the depth tracker
                depthTracker = plotList[split].depth;               
            }
        }
        
        private void splitPlot(Plot parentPlot, SplitOrientation splitOrientation) {
            Plot childAPlot;
            Plot childBPlot;
            
            int splitAt;
            float splitFactor;
            float splitValue;

            int childADepth;
            int childARow;
            int childACol;
            int childAHeight;
            int childAWidth;

            int childBDepth;
            int childBRow;
            int childBCol;
            int childBHeight;
            int childBWidth;

            splitFactor = randomIdentityRange(splitVariable, 1 - splitVariable);

            // determine splits and update Fields
            if (splitOrientation == SplitOrientation.Vertical) {
                splitValue = splitFactor * parentPlot.width;
                splitAt = parentPlot.width - (int) splitValue;
                
                childAWidth = parentPlot.width - splitAt;
                childBWidth = parentPlot.width - childAWidth;
                
                childAHeight = parentPlot.height;
                childBHeight = childAHeight;
                
                childARow = parentPlot.row;              
                childBRow = childARow;
                
                childACol = parentPlot.col;
                childBCol = parentPlot.col + childAWidth;
            }
            else {
                splitValue = splitFactor * parentPlot.height;
                splitAt = parentPlot.height - (int) splitValue;
                
                childAHeight = parentPlot.height - splitAt;
                childBHeight = parentPlot.height - childAHeight;
                
                childAWidth = parentPlot.width;
                childBWidth = childAWidth;
                
                childARow = parentPlot.row;
                childBRow = parentPlot.row + childAHeight; 
                
                childACol = parentPlot.col;                              
                childBCol = childACol;
            }

            // update other data
            childADepth = parentPlot.depth + 1;
            childBDepth = parentPlot.depth + 1;
            
            // initialize children
            childAPlot = new Plot(childADepth, childARow, childACol, childAHeight, childAWidth);
            childBPlot = new Plot(childBDepth, childBRow, childBCol, childBHeight, childBWidth);
            
            // update parent node with its new children
            parentPlot.children.Add(childAPlot);
            parentPlot.children.Add(childBPlot);
            
            // add children to plot list
            plotList.Add(childAPlot);
            plotList.Add(childBPlot);
        }
        
        
        // Currently the tree is hardcoded since 16 plots per floor is a good balance for what the game is doing.
        // If you want to generate more plots per floor in the future, you'll have to generalize the tree instead of hardcoding.
        // Also, when you come back to this later for the outdoor/directional areas, this will have to be either generalized or
        // handled differently.
        private void generateRooms() {
            List<Plot> quadrantPlots;
            List<Plot> roomPlots;
            Plot currentQuadrantPlot;
            Plot chooseRoomPlot;
            int occupiedCount;

            // add quadrant plots to list
            quadrantPlots = new List<Plot> {
                plotList[3],
                plotList[4],
                plotList[5],
                plotList[6]
            };
            
            // add room plots to list
            roomPlots = new List<Plot>(); 
            for (int plot = 15; plot < 31; plot++) {
                roomPlots.Add(plotList[plot]);
            }
            
            // randomly pick a quadrant plot for the starting point
            int quadrantPlot = random.Next() % 4;
            currentQuadrantPlot = quadrantPlots[quadrantPlot];
            
            // distribute room in a round robin fasion, starting at the currentQuadronPlot
            occupiedCount = 0;
            
            for (int count = 0; count < numberOfRooms; count++) {
                // Randomly choose a roomPlot from the currentQuadrantPlot that isn't occupied
                // This isn't the best implementation since it's randomly choosing between the available plots
                // over and over until it strikes an empty one. If the generator is slow for some reason,
                // implement this sequentially instead of randomly.
                chooseRoomPlot = currentQuadrantPlot.children[random.Next() % 2].children[random.Next() % 2];
                while (chooseRoomPlot.isOccupied) {
                    chooseRoomPlot = currentQuadrantPlot.children[random.Next() % 2].children[random.Next() % 2];
                }
                 
                // BUILD REGULAR ROOM
                buildRegularRoomOrHallpoint(chooseRoomPlot, "Room");
                
                // round robin to the next quadrant plot
                quadrantPlot = (quadrantPlot + 1) % 4;
                currentQuadrantPlot = quadrantPlots[quadrantPlot];
                
                // update occupied count
                occupiedCount++;               
            }
            
            
            // DISTRIBUTE HALLS AND BLANKS
            // Again, later you'll have to generalize 16 instead of hardcoding the number of room plots.
            // Current this is set to a 50/50 chance of generating a blank or a hall, of course you can and maybe
            // should change or give controls to this setting later.
            for (int count = 0; count < 16 - occupiedCount; count++) {
                // Randomly choose a roomPlot from the currentQuadrantPlot that isn't occupied
                // This isn't the best implementation since it's randomly choosing between the available plots
                // over and over until it strikes an empty one. If the generator is slow for some reason,
                // implement this sequentially instead of randomly.
                chooseRoomPlot = currentQuadrantPlot.children[random.Next() % 2].children[random.Next() % 2];
                while (chooseRoomPlot.isOccupied) {
                    chooseRoomPlot = currentQuadrantPlot.children[random.Next() % 2].children[random.Next() % 2];
                }
                       
                int choose = random.Next() % 2;
                // make current plot a blank if 0
                if (choose == 0) {
                    
                
                }
                // make current plot a hall if 1
                else if (choose == 1) {
                    buildRegularRoomOrHallpoint(chooseRoomPlot, "Hall");
                }
                
                // round robin to the next quadrant plot
                quadrantPlot = (quadrantPlot + 1) % 4;
                currentQuadrantPlot = quadrantPlots[quadrantPlot];
            }
        }

        
        // Hallpoints are just treated as a 1x1 room
        void buildRegularRoomOrHallpoint(Plot chooseRoomPlot, String choice) {
            int buildSpaceRow;
            int buildSpaceCol;
            int buildSpaceHeight;
            int buildSpaceWidth;

            int roomRow;
            int roomCol;
            int roomHeight;
            int roomWidth;

            int midpointRestrictionSpaceRow;
            int midpointRestrictionSpaceCol;
            int midpointRestrictionSpaceHeight;
            int midpointRestrictionSpaceWidth;

            Hallmark hallmarkNorth;
            Hallmark hallmarkSouth;
            Hallmark hallmarkEast;
            Hallmark hallmarkWest;

            bool isHallpoint;

            if (choice == "Room") {
                isHallpoint = false;
            }
            else {
                isHallpoint = true;
            }
              
            // Define Build Space
            buildSpaceRow = chooseRoomPlot.row + 2;
            buildSpaceCol = chooseRoomPlot.col + 2;
            buildSpaceHeight = chooseRoomPlot.height - 4;
            buildSpaceWidth = chooseRoomPlot.width - 4;
            
            // Define Room
            if (choice.Equals("Room")) {
                roomHeight = randomRange(minRoomHeight, maxRoomHeight < buildSpaceHeight ? maxRoomHeight : buildSpaceHeight);
                roomWidth = randomRange(minRoomWidth, maxRoomWidth < buildSpaceWidth ? maxRoomWidth : buildSpaceWidth);
                roomRow = randomRange(buildSpaceRow, (buildSpaceRow + buildSpaceHeight) - roomHeight);
                roomCol = randomRange(buildSpaceCol, (buildSpaceCol + buildSpaceWidth) - roomWidth);
            }
            // Else choice is "Hall";
            else {
                roomHeight = 1;
                roomWidth = 1;
                roomRow = randomRange(buildSpaceRow, (buildSpaceRow + buildSpaceHeight) - roomHeight);
                roomCol = randomRange(buildSpaceCol, (buildSpaceCol + buildSpaceWidth) - roomWidth);
            }

            // define midpoint restriction zone
            if (choice.Equals("Room")) {
                midpointRestrictionSpaceRow = roomRow - 1;
                midpointRestrictionSpaceCol = roomCol - 1;
                midpointRestrictionSpaceHeight = roomHeight + 2;
                midpointRestrictionSpaceWidth = roomWidth + 2;
            }
            // Else choice is "Hall"
            else {
                midpointRestrictionSpaceRow = roomRow;
                midpointRestrictionSpaceCol = roomCol;
                midpointRestrictionSpaceHeight = 1;
                midpointRestrictionSpaceWidth = 1;
            }

            // define hallmarks
            if (choice.Equals("Room")) {
                hallmarkNorth = new Hallmark(
                    HallmarkType.North,
                    roomRow,
                    randomRange(roomCol + 1, (roomCol + roomWidth) - 2)
                );
                hallmarkSouth = new Hallmark(
                    HallmarkType.South,
                    roomRow + roomHeight - 1,
                    randomRange(roomCol + 1, (roomCol + roomWidth) - 2)
                );
                hallmarkEast = new Hallmark(
                    HallmarkType.East,
                    randomRange(roomRow + 1, (roomRow + roomHeight) - 2),
                    roomCol + roomWidth - 1
                );
                hallmarkWest = new Hallmark(
                    HallmarkType.West,
                    randomRange(roomRow + 1, (roomRow + roomHeight) - 2),
                    roomCol
                );
            }
            // Else choice is "Hall"
            else {
                hallmarkNorth = new Hallmark(
                    HallmarkType.North,
                    roomRow,
                    roomCol
                );
                hallmarkSouth = new Hallmark(
                    HallmarkType.South,
                    roomRow,
                    roomCol
                );
                hallmarkEast = new Hallmark(
                    HallmarkType.East,
                    roomRow,
                    roomCol
                );
                hallmarkWest = new Hallmark(
                    HallmarkType.West,
                    roomRow,
                    roomCol
                );
            }

            // build room
            Room room = new Room(
                buildSpaceRow,
                buildSpaceCol,
                buildSpaceHeight,
                buildSpaceWidth,

                roomRow,
                roomCol,
                roomHeight,
                roomWidth,

                midpointRestrictionSpaceRow,
                midpointRestrictionSpaceCol,
                midpointRestrictionSpaceHeight,
                midpointRestrictionSpaceWidth,

                hallmarkNorth,
                hallmarkSouth,
                hallmarkEast,
                hallmarkWest,
                
                isHallpoint);
            
            // link hallmarks to the room
            hallmarkNorth.room = room;
            hallmarkSouth.room = room;
            hallmarkEast.room = room;
            hallmarkWest.room = room;
                           
            // add halmarks to hallmark list
            chooseRoomPlot.hallmarks.Add(hallmarkNorth);
            chooseRoomPlot.hallmarks.Add(hallmarkSouth);
            chooseRoomPlot.hallmarks.Add(hallmarkEast);
            chooseRoomPlot.hallmarks.Add(hallmarkWest);
            
            // add room to plot roomlist
            chooseRoomPlot.rooms.Add(room);
            
            // set isOccupied to true
            chooseRoomPlot.isOccupied = true;
            
            // update midpoint restriction map
            for (int row = midpointRestrictionSpaceRow; row < midpointRestrictionSpaceRow + midpointRestrictionSpaceHeight; row++) {
                for (int col = midpointRestrictionSpaceCol; col < midpointRestrictionSpaceCol + midpointRestrictionSpaceWidth; col++) {
                    midpointRestrictionMap[row, col] = true;
                }
            }
        }


        private void debugPrint() {
            drawDebugRooms();
            printMap();
        }
        
        public List<HallmarkDistance> addHallmarkAndSort(Hallmark from, List<HallmarkDistance> hallmarkDistances, Plot child1) {
            foreach (Hallmark to in child1.hallmarks) {
                // check that the distance is measuring something valid to connect to,
                // for example, a north hallmark isn't going to connect to a south hallmark with a row value that is higher
                if (   (from.type == HallmarkType.North && to.type == HallmarkType.South && from.row > to.row && !to.connected && Math.Abs(from.row - to.row) >= 4)
                    || (from.type == HallmarkType.South && to.type == HallmarkType.North && from.row < to.row && !to.connected && Math.Abs(from.row - to.row) >= 4) 
                    || (from.type == HallmarkType.East && to.type == HallmarkType.West && from.col < to.col && !to.connected && Math.Abs(from.col - to.col) >= 4)
                    || (from.type == HallmarkType.West && to.type == HallmarkType.East && from.col > to.col && !to.connected && Math.Abs(from.col - to.col) >= 4)) {
                    
                    // create hallmarkDistance with hallmarks and add them to the list
                    hallmarkDistances.Add(new HallmarkDistance(from, to, squareDistance(from, to)));
                }
            }

            return hallmarkDistances;
        }
        

        private void connectHalls(Plot plot) {
            if (plot.children[0].hasChildren()) {
                connectHalls(plot.children[0]);
            }

            if (plot.children[1].hasChildren()) {
                connectHalls(plot.children[1]);
            }
                  
            // Get sorted list of all Hallmarks between childA and childB
            List<HallmarkDistance> hallmarkDistances = new List<HallmarkDistance>();
            
            // does several checks
            // 1) check that the hallmark isn't already connected
            // 2) check which type of hallmark it is
            // 3) check that the distance is measuring something valid to connect to,
            // for example, a north hallmark isn't going to connect to a south hallmark with a row value that is higher
            // (this is done in the addHallmarkAndSort() function
            foreach (Hallmark from in plot.children[0].hallmarks) {
                if (!from.connected) {
                    if (from.type == HallmarkType.North) {
                        hallmarkDistances = addHallmarkAndSort(from, hallmarkDistances, plot.children[1]);
                    }
                    else if (from.type == HallmarkType.South) {
                        hallmarkDistances = addHallmarkAndSort(from, hallmarkDistances, plot.children[1]);
                    }
                    else if (from.type == HallmarkType.East) {
                        hallmarkDistances = addHallmarkAndSort(from, hallmarkDistances, plot.children[1]);
                    }
                    else if (from.type == HallmarkType.West) {
                        hallmarkDistances = addHallmarkAndSort(from, hallmarkDistances, plot.children[1]);
                    }
                }
            }
            
            // sort hallmark distances from shortest to longest
            hallmarkDistances = hallmarkDistances.OrderBy(hallmark => hallmark.distance).ToList();
            
            
            // Connect Rooms with shortest distances
            int chance = random.Next() % 2;
            
            if (hallmarkDistances.Any()) {
                if (hallmarkDistances.Count < 4 || chance == 0) {
                    
                    connectHallmarks(plot, hallmarkDistances[0].from, hallmarkDistances[0].to);
                    hallmarkDistances[0].from.connected = true;
                    hallmarkDistances[0].to.connected = true;
                }
                else {
                    for (int i = 0; i < 2; i++) {
                        if (!hallmarkDistances[i].from.connected && !hallmarkDistances[i].to.connected) {
                            
                            connectHallmarks(plot, hallmarkDistances[i].from, hallmarkDistances[i].to);
                            hallmarkDistances[i].from.connected = true;
                            hallmarkDistances[i].to.connected = true;
                        }
                    }
                }
            }

            // pass hallmarks from children nodes to parent node
            for (int child = 0; child < 2; child++) {
                foreach (Hallmark hallmark in plot.children[child].hallmarks) {
                    plot.hallmarks.Add(hallmark);
                }
            }
            
            // pass halls
            for (int child = 0; child < 2; child++) {
                foreach (Hall hall in plot.children[child].halls) {
                    plot.halls.Add(hall);
                }
            }
            
            // pass rooms
            for (int child = 0; child < 2; child++) {
                foreach (Room room in plot.children[child].rooms) {
                    plot.rooms.Add(room);
                }
            }
        }      
        
        
        
        
        
        public void connectHallmarks(Plot currentPlot, Hallmark from, Hallmark to) {
            int rowDirection;
            int colDirection;
            Hall hall;

            // rowDirection and colDirection are identity values which determine the direction of traversal
            rowDirection = to.row - from.row > 0 ? 1 : to.row - from.row < 0 ? -1 : 0;
            colDirection = to.col - from.col > 0 ? 1 : to.col - from.col < 0 ? -1 : 0;
            
            //debugPrint();
            //printMap();
            // Pick a random midpoint between from and to
            // get the distance in magnitude from one hallmark to the other and put the midpoint at halfway
            int midpointRow = Math.Abs((from.row + 1 * rowDirection) + (to.row - 1 * rowDirection)) / 2;
            int midpointCol = Math.Abs((from.col + 1 * colDirection) + (to.col - 1 * colDirection)) / 2;
            
     
            //map[midpointRow, midpointCol] = 'O';
                       
            // build hall
            hall = buildHall(from, to, midpointRow, midpointCol, rowDirection, colDirection);
            currentPlot.halls.Add(hall);

            // set hall references in hallmarks
            from.hall = hall;
            to.hall = hall;
        }
        
        
        public Hall buildHall(Hallmark from, Hallmark to, int midpointRow, int midpointCol, int rowDirection, int colDirection) {
            int currentRow;
            int currentCol;
            List<int[]> hallpath;

            currentRow = from.row;
            currentCol = from.col;
            hallpath = new List<int[]>();
            
            if (from.type == HallmarkType.North || from.type == HallmarkType.South) {
                for (int r = 0; r < Math.Abs(from.row - midpointRow); r++) {
                    currentRow += 1 * rowDirection;
                    if (r > 0) {
                        hallpath.Add(new int[] {currentRow, currentCol});
                    }
                }
                for (int c = 0; c < Math.Abs(from.col - midpointCol); c++) {
                    currentCol += 1 * colDirection;
                    hallpath.Add(new int[] {currentRow, currentCol});
                }for (int c = 0; c < Math.Abs(midpointCol - to.col); c++) {
                    currentCol += 1 * colDirection;
                    hallpath.Add(new int[] {currentRow, currentCol});
                }
                // -1 keeps the hall from going into the room
                for (int r = 0; r < Math.Abs(midpointRow - to.row) - 1; r++) {
                    currentRow += 1 * rowDirection;
                    if (r < Math.Abs(midpointRow - to.row) - 2) {
                        hallpath.Add(new int[] {currentRow, currentCol});
                    }
                }               
            }
            else {
                for (int c = 0; c < Math.Abs(from.col - midpointCol); c++) {
                    currentCol += 1 * colDirection;
                    if (c > 0) {
                        hallpath.Add(new int[] {currentRow, currentCol});
                    }
                }
                for (int r = 0; r < Math.Abs(from.row - midpointRow); r++) {
                    currentRow += 1 * rowDirection;
                    hallpath.Add(new int[] {currentRow, currentCol});
                }
                for (int r = 0; r < Math.Abs(midpointRow - to.row); r++) {
                    currentRow += 1 * rowDirection;
                    hallpath.Add(new int[] {currentRow, currentCol});
                } 
                // -1 keeps the hall from going into the room
                for (int c = 0; c < Math.Abs(midpointCol - to.col) - 1; c++) {
                    currentCol += 1 * colDirection;
                    if (c < Math.Abs(midpointCol - to.col) - 2) {
                        hallpath.Add(new int[] {currentRow, currentCol});
                    }
                }              
            }
            
            return new Hall(hallpath, from, to);
        }
        
        public double squareDistance(Hallmark from, Hallmark to) {
            double a = from.row - to.row;
            double b = from.col - to.col;

            return Math.Sqrt(a * a + b * b);
        }
         
        public void drawDebugRooms() {
            //drawPlots();
            //drawRoomSpaces();
            drawRooms();
            //drawHallmarks();
            drawHalls();
            drawEntrances();
            drawWalls();
            //drawMidpointRestrictionSpaces();

            //drawHallmarkZones();
            //drawHallmarks();
        }
        
        private void drawWalls() {
            foreach (Room room in plotList[0].rooms) {
                foreach (int[] pos in room.walls) {
                    map[pos[0], pos[1]] = '▓';
                }
            }
        }

        private void drawEntrances() {
            foreach (Room room in plotList[0].rooms) {
                foreach (int[] pos in room.entrances) {
                    map[pos[0], pos[1]] = '~';
                }
            }
        }
        
        private void drawRoomSpaces() {
            foreach (Plot plot in plotList) {
                foreach (Room room in plot.rooms) {
                    drawSpace(room.buildSpaceRow, room.buildSpaceCol, room.buildSpaceHeight, room.buildSpaceWidth, ' ');
                }
            }
        }
     
        public void drawRooms() {
            foreach (Room room in plotList[0].rooms) {
                drawSpace(room.roomRow, room.roomCol, room.roomHeight, room.roomWidth, '.');
            }
            
        }

        public void drawHalls() {
            foreach (Hall hall in plotList[0].halls) {
                foreach (int[] hallpoint in hall.hallpoints) {
                    map[hallpoint[0], hallpoint[1]] = ',';
                }
            }
        }

        public void drawHallmarks() {
            char ch = 'x';
            
            foreach (Plot plot in plotList) {
                foreach (Room room in plot.rooms ) {               
                    map[room.hallmarkNorth.row, room.hallmarkNorth.col] = ch;
                    map[room.hallmarkSouth.row, room.hallmarkSouth.col] = ch;
                    map[room.hallmarkEast.row, room.hallmarkEast.col] = ch;
                    map[room.hallmarkWest.row, room.hallmarkWest.col] = ch;
                }
            }
        }
        
        public void drawSpace(int row, int col, int height, int width, Char character) {
            for (int r = row; r < row + height; r++) {
                for (int c = col;  c < col + width; c++) {
                    map[r, c] = character;
                }
            }
        }

        private void drawPlots() {
            for (int plot = 0; plot < plotList.Count; plot++) {
                if (!plotList[plot].hasChildren()) {
                    for (int row = plotList[plot].row; row < plotList[plot].row + plotList[plot].height; row++) {
                        for (int col = plotList[plot].col; col < plotList[plot].col + plotList[plot].width; col++) {
                            map[row, col] = (char)(plot + 32);
                        }
                    }
                }
            }
        }
        
        private void printMap() {
            for (int row = 0; row < map.GetLength(0); row++) {
                for (int col = 0; col < map.GetLength(1); col++) {
                    Console.Write(map[row, col] + "  ");
                }

                Console.Write('\n');
            }
        }

        private void p(String name, String value) {
            Console.WriteLine(name + ": " + value);
        }

        private SplitOrientation switchOrientation(SplitOrientation orientation) {
            return orientation == SplitOrientation.Horizontal ? SplitOrientation.Vertical : SplitOrientation.Horizontal;
        }

        private float randomIdentityRange(float min, float max) {
            double range = max - min;
            double sample = random.NextDouble();
            double scaled = sample * range + min;
            return (float) scaled;
        }
        
        public int randomRange(int min, int max) {            
            int sample = min == 0 ? 0 : random.Next() % (Math.Abs(max - min) + 1);
            return max - sample;
        }
    }

    public enum SplitOrientation {
        Horizontal,
        Vertical,
        None
    }

    public class Plot {
        public int ID;
        public int depth;
        public int row;
        public int col;
        public int height;
        public int width;
        public List<int[]> midpoints;
        public List<Room> rooms;
        public List<Hall> halls;
        public List<Plot> children;
        public List<Hallmark> hallmarks;
        public SplitOrientation splitOrientation;
        public bool isOccupied;

        public Plot(int depth, int row, int col, int height, int width) {
            this.depth = depth;
            this.row = row;
            this.col = col;
            this.height = height;
            this.width = width;
            rooms = new List<Room>();
            halls = new List<Hall>();
            children = new List<Plot>();         
            hallmarks = new List<Hallmark>();
            splitOrientation = SplitOrientation.None;
            isOccupied = false;
        }

        public bool hasChildren() {
            return children.Any();
        }
    }

    public enum HallmarkType {
        North,
        South,
        East,
        West
    }

    public class Hallmark {
        public HallmarkType type;
        public int row;
        public int col;
        public Room room;
        public Hall hall;
        public bool connected;

        public Hallmark(HallmarkType type, int row, int col) {
            this.type = type;
            this.row = row;
            this.col = col;
            connected = false;
        }
    }

    public class HallmarkDistance {
        public Hallmark from;
        public Hallmark to;        
        public double distance;

        public HallmarkDistance(Hallmark from, Hallmark to, double distance) {
            this.from = from;
            this.to = to;
            this.distance = distance;
        }
    }

    public class Hall {
        public List<int[]> hallpoints;
        public List<int[]> walls;
        public Hallmark from;
        public Hallmark to;

        public Hall(List<int[]> hallpoints, Hallmark from, Hallmark to) {
            this.hallpoints = hallpoints;
            this.from = from;
            this.to = to;
            walls = new List<int[]>();
        }
    }
    
    public class Room {
        public int buildSpaceRow;
        public int buildSpaceCol;
        public int buildSpaceHeight;
        public int buildSpaceWidth;

        public int roomRow;
        public int roomCol;
        public int roomHeight;
        public int roomWidth;

        public int midpointRestrictionSpaceRow;
        public int midpointRestrictionSpaceCol;
        public int midpointRestrictionSpaceHeight;
        public int midpointRestrictionSpaceWidth;

        public Hallmark hallmarkNorth;
        public Hallmark hallmarkSouth;
        public Hallmark hallmarkEast;
        public Hallmark hallmarkWest;

        public List<Hallmark> hallmarks;

        public bool isHallpoint;

        public List<int[]> entrances;
        public List<int[]> walls;


        public Room(
                 int buildSpaceRow,
                 int buildSpaceCol,
                 int buildSpaceHeight,
                 int buildSpaceWidth,
        
                 int roomRow,
                 int roomCol,
                 int roomHeight,
                 int roomWidth,
        
                 int midpointRestrictionSpaceRow,
                 int midpointRestrictionSpaceCol,
                 int midpointRestrictionSpaceHeight,
                 int midpointRestrictionSpaceWidth,
        
                 Hallmark hallmarkNorth,
                 Hallmark hallmarkSouth,
                 Hallmark hallmarkEast,
                 Hallmark hallmarkWest,
                 
                 bool isHallpoint) {

            this.buildSpaceRow = buildSpaceRow;
            this.buildSpaceCol = buildSpaceCol;
            this.buildSpaceHeight = buildSpaceHeight;
            this.buildSpaceWidth = buildSpaceWidth;

            this.roomRow = roomRow;
            this.roomCol = roomCol;
            this.roomHeight = roomHeight;
            this.roomWidth = roomWidth;

            this.midpointRestrictionSpaceRow = midpointRestrictionSpaceRow;
            this.midpointRestrictionSpaceCol = midpointRestrictionSpaceCol;
            this.midpointRestrictionSpaceHeight = midpointRestrictionSpaceHeight;
            this.midpointRestrictionSpaceWidth = midpointRestrictionSpaceWidth;

            this.hallmarkNorth = hallmarkNorth;
            this.hallmarkSouth = hallmarkSouth;
            this.hallmarkEast = hallmarkEast;
            this.hallmarkWest = hallmarkWest;

            hallmarks = new List<Hallmark> {
                hallmarkNorth,
                hallmarkSouth,
                hallmarkEast,
                hallmarkWest
            };

            entrances = new List<int[]>();
            walls = new List<int[]>();

            this.isHallpoint = isHallpoint;
        }
    }   
}

