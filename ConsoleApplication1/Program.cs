﻿using System;
using ConsoleApplication1;

// ReSharper disable All

internal class Program {
    
    public static void Main() {
        Random random = new Random();
        
        int padding = 4;
        int numberOfRooms =  7 + (random.Next() % 6);
        int minRoomHeight = 3;
        int minRoomWidth = 3;
        int maxRoomHeight = 4 + (random.Next() % 3);
        int maxRoomWidth = 4 + (random.Next() % 3);
        bool connectEverything = false; // work this in later
        bool allowDeadends = false; // work this in later
        float splitFactor = 0.5f;

        Floor floor = new Floor(minRoomHeight, minRoomWidth, maxRoomHeight, maxRoomWidth, numberOfRooms, splitFactor, padding);
        // floor = process(floor); // this stuff would walla and assign tile info into the map, then hand it off to unity
    }
}
